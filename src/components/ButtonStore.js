import React, { Component } from "react";

export default function ButtonStore({deal,storeObj}) {
//"YMRm%2FCVtlmXhrcsTmmE6A3%2BzIdxAI8tbklGgEd4L9wc%3D"
  return (
    <a href={`https://www.cheapshark.com/redirect?dealID=${deal.dealID}`} 
    className="btn btn btn-link text-start px-3 mb-2">
    {storeObj.storeName}
    </a>
  );
}
