import React, { Component } from "react";
import ButtonStore from "./ButtonStore";
/**
 * Lista de juegos
 *
 *
 */
export default function GamesList({ juegos, stores }) {
  
  const getStore = (stores,storeID) => {
    for(let i=0;i<stores.length;i++){
      let stor = stores[i];
      if(stor.storeID == storeID){
       return stor;
      }
    }
  }
  
  const limiterGame = (deals) =>{
    if(deals.length>3){
      return [deals[0],deals[1],deals[2]];
    }else{
      return deals;
    }
  }


  /**
   * Funcion para renderizar
   */
  return (
    /**
     * Contenedor de las Cartas.
     */
    <div className="row d-flex justify-content-center">
      {juegos.map((juego, index) => (
        // <li key={index} >{juego.info.title}</li>
        <div className="card mb-3 me-3 col-md-5 " key={index}>
          <div className="row g-0">
            <div className="col-md-4">
              <img
                src={juego.info.thumb}
                className="img-fluid rounded-start img-cover"
                alt="..."
              />
            </div>
            <div className="col-md-8">
              <div className="card-body d-flex flex-column justify-content-start">
                <h5 className="card-title text-start">{juego.info.title}</h5>
                <div className="btn-group-vertical">
                  {
                    limiterGame(juego.deals).map((deal,index2) => (
                      <ButtonStore key={index2} deal={deal} storeObj={getStore(stores,deal.storeID)} />
                    ))
                    //ButtonStore
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}
