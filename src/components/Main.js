import React, { Component } from "react";
import BodyContainer from "./BodyContainer";
export default function Main() {
  return(
  <>
    <section>
      <main>
        <BodyContainer />
      </main>
    </section>
  </>);
}
