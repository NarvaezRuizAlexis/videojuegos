import { setSelectionRange } from "@testing-library/user-event/dist/utils";
import React, { useState, useEffect } from "react";
import GamesList from "./GamesList";
/***
 * Componente prinicpal para el input de busqeda.
 *
 */
export default function BodyContainer() {
  /**
   * Hooks
   */
  const [gamesState, setGamesList] = useState([]);
  const [stores, setStore] = useState(null);
  const [classesInput, setStyleInput] = useState(
    "container new-height d-flex justify-content-center align-items-center"
  );

  /**
   * Funbcion que se ejecuta despues del pintado del componente.
   */
  useEffect(() => {
    getStores();
  }, []);

  /**
   * Funcion para realizar la peticion a la API.
   */
  const getGames = () => {
    //Realizamos la peticion a la API.
    let nameGame = document.getElementById("idInputSearch");
    //https://www.cheapshark.com/api/1.0/deals?sortBy=Price&title=batman
    let urlPrimaria = `https://www.cheapshark.com/api/1.0/deals?sortBy=Price&title=${nameGame.value}`;
    let urlSecundaria = `https://www.cheapshark.com/api/1.0/games?ids=`;

    if (nameGame.value) {
      getIDsGames(urlPrimaria).then((idsGames) => {
        getDeals(urlSecundaria, idsGames).then((response) => {
          let tempArray = Object.keys(response).map((key) => response[key]);
          setGamesList((prev) => tempArray);
          setStyleInput(
            (prev) =>
              "container new-height d-flex justify-content-center align-items-center active"
          );
        });
      });
    }
  };

  /**
   * Obtenemos todos los juegos disponibles con el nombre que busquemos
   */
  const getIDsGames = async (url) => {
    let idsGames = [];
    await fetch(url)
      .then((response) => response.json())
      .then((games) => {
        let tempArray = Object.keys(games).map((key) => games[key]);
        let idsGamesTemp = tempArray.map((e) => e.gameID);
        let idsGamesReduce = idsGamesTemp.filter(
          (game, pos) => idsGamesTemp.indexOf(game) == pos
        );
        idsGames = idsGamesReduce;
      });

    return idsGames;
  };
  /**
   * Obtenemos todos los descuentos sombre los juegos.
   */

  const getDeals = async (url, ids) => {
    //console.log(`${url}${ids}`)
    let gamesFinal = [];
    await fetch(`${url}${ids}`)
      .then((response) => response.json())
      .then((games) => {
        //Tranformamos los juegos (JSON) en un array
        Object.keys(games)
          .map((key) => games[key])
          .forEach((element) => {
            gamesFinal.push(element);
          });
      });
    return gamesFinal;
  };

  const getStores = async () => {
    let STORES = [];
    const URL_STORES = "https://www.cheapshark.com/api/1.0/stores";
    //let storesArray = Object.keys(storesJSON).map((key) => storesJSON[key]);
    const query = await fetch(URL_STORES);
    let stors = await query.json();
    setStore(stors);
  };

  /**
   * Funciona para el renderizado.
   *
   */
  return (
    <>
      <section className={classesInput + " mb-4"}>
        <div className="row w-100 justify-content-center">
          <div className="col-md-8 col-sm-10">
            <div className="input-group">
              <input
                type="text"
                id="idInputSearch"
                className="form-control px-3"
              />
              <button
                type="button"
                className="btn btn-outline-secondary px-3"
                onClick={getGames}
              >
                ¡Voy a tener suerte!
              </button>
            </div>
          </div>
        </div>
      </section>
      <section id="litaJuegos" className="container">
        <GamesList juegos={gamesState} stores={stores} />
      </section>
    </>
  );
}
