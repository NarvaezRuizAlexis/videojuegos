import React from 'react'
/**
 * Input principal de la pagina
 */
export default function InputPage({getGames}) {
    return (
        <div className="container">
            <input type="text" id="idInputSearch" />
            <button type="button" onClick={()=>{getGames()}}>
            Buscar
            </button>
        </div>
    )
}
