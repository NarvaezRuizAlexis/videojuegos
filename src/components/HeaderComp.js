import React, { Component } from "react";
/**
 * @autor Narvaez Ruiz ALexis
 * Header de las paginas
 *
 */
export default class HeaderComp extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <header className="container">
        <nav className="navbar navbar-expand-md navbar-light bg-white">
          <div className="container-fluid">
            <span className="navbar-brand h1">Logo</span>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="true"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <a className="nav-link" aria-current="page" href="../">
                    Inicio
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/AcercaDe">
                    Acerca de
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    );
  }
}
