import logo from "./logo.svg";
import AcercaDe from "./components/AcercaDe";
import Main from "./components/Main";
import HeaderComp from "./components/HeaderComp";
import FooterComp from "./components/FooterComp";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";

function App() {
  return (
    <Router>
      <div className="App d-flex flex-column vh-100">
        {/*Seccion del header de la pagina principal*/}
        <section className="">
          <HeaderComp />
        </section>
        {/*Seccion del "main" de la pagina principal*/}
        <Routes>
          <Route exact path="/" element={<Main>/</Main>} />
          <Route exact path="/AcercaDe" element={<AcercaDe>/</AcercaDe>} />
        </Routes>
        {/*Seccion del "main" de la pagina principal*/}
        <section className="container mt-auto">
          <FooterComp />
        </section>
      </div>
    </Router>
  );
}

export default App;

/**
  <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
  */
